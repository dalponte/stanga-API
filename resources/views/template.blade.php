<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Projetos de arquitetura online">
    <meta name="author" content="Trendsoft">
    <meta name="keyword" content="Arquitetura,Arquiteto,Arquitetos,Serviço,Projeto,Online">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title><?= Config::get('app.name')?></title>
    <link rel="shortcut icon" href="<?= URL::asset('img/favicon.png')?>">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/datatables.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/dashboard.css')}}">
</head>

<body>
<div class="container-fluid p-0">
    @yield('content')
</div>

<script src="<?= URL::asset('js/jquery-3.2.1.min.js') ?>"></script>
<script src="<?= URL::asset('js/plugins/popper.min.js') ?>"></script>
<script src="<?= URL::asset('js/bootstrap.js') ?>"></script>
<script src="<?= URL::asset('js/jquery-renderOn-1.0.1.js') ?>"></script>
<script src="<?= URL::asset('js/plugins/datatables.min.js') ?>"></script>
<script src="<?= URL::asset('js/app.js') ?>"></script>
@yield('js')

</body>
</html>
