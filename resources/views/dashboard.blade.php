@extends('template')

@section('content')

    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">API from hell</a>

    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                                <span class="fa fa-home"></span>
                                Dashboard <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">
                                <span class="fa fa-map"></span>
                                Mapeamento
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="fa fa-users"></span>
                                Usuários
                            </a>
                        </li>
                    </ul>

                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Relatórios</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                            <span class="fa fa-plus-circle"></span>
                        </a>
                    </h6>
                    <ul class="nav flex-column mb-2">
                        <li class="nav-item">
                            <a class="nav-link text-muted">
                                <small><i>Mais depois</i></small>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h4 class="h4">Listagem de dados</h4>
                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group mr-2">
                            <button class="btn btn-sm btn-outline-secondary disabled">Share</button>
                            <button class="btn btn-sm btn-outline-secondary disabled">Export</button>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-sm ">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Equipamento</th>
                            <th>Valor</th>
                            <th>Data</th>
                            <th>Metadados</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Status::orderBy('id', 'DESC')->get() as $s)
                            <tr>
                                <td>{{$s->id}}</td>
                                <td>{{$s->equipamento}}</td>
                                <td>{{$s->status}}</td>
                                <td>{{$s->created_at}}</td>
                                <td><?php dump($s->metadados) ?></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </main>
        </div>
    </div>


@endsection
